; download dependencies for the Workstation site
core = 7.x

api = 2
projects[] = "drupal"

;;
;; Site core functionality
;;

;; current release always good ;;
projects[views][type] = "module"
projects[views][subdir] = "contrib/"

projects[embed_views][version] = 1.2
projects[embed_views][type] = "module"
projects[embed_views][subdir] = "contrib/"

projects[eva][version] = 1.2
projects[eva][type] = "module"
projects[eva][subdir] = "contrib/"

projects[views_field_view][version] = 1.1
projects[views_field_view][type] = "module"
projects[views_field_view][subdir] = "contrib/"

projects[views_bulk_operations][version] = 3.1
projects[views_bulk_operations][type] = "module"
projects[views_bulk_operations][subdir] = "contrib/"

projects[ctools][type] = "module"
projects[ctools][subdir] = "contrib/"

projects[context][type] = "module"
projects[context][subdir] = "contrib/"

projects[date][version] = 2.6
projects[date][type] = "module"
projects[date][subdir] = "contrib/"

projects[ds][type] = "module"
projects[ds][subdir] = "contrib/"

projects[fences][type] = "module"
projects[fences][subdir] = "contrib/"

projects[ckeditor][version] = 1.x-dev
projects[ckeditor][type] = "module"
projects[ckeditor][subdir] = "contrib/"

projects[plupload][version] = 1.2
projects[plupload][type] = "module"
projects[plupload][subdir] = "contrib/"

projects[image_resize_filter][type] = "module"
projects[image_resize_filter][subdir] = "contrib/"

projects[insert][type] = "module"
projects[insert][subdir] = "contrib/"

projects[htmlpurifier][type] = "module"
projects[htmlpurifier][subdir] = "contrib/"

projects[nodeformsettings][version] = 2.x-dev
projects[nodeformsettings][type] = "module"
projects[nodeformsettings][subdir] = "contrib/"

projects[field_group][version] = 1.x-dev
projects[field_group][type] = "module"
projects[field_group][subdir] = "contrib/"

projects[better_formats][version] = 1.0-beta1
projects[better_formats][type] = "module"
projects[better_formats][subdir] = "contrib/"

projects[publish_button][version] = 1.x-dev
projects[publish_button][type] = "module"
projects[publish_button][subdir] = "contrib/"

projects[views_ui_basic][version] = 1.2
projects[views_ui_basic][type] = "module"
projects[views_ui_basic][subdir] = "contrib/"

projects[views_php][type] = "module"
projects[views_php][subdir] = "contrib/"

projects[video_embed_field][version] = 2.0-beta5
projects[video_embed_field][type] = "module"
projects[video_embed_field][subdir] = "contrib/"

projects[rules][version] = 2.3
projects[rules][type] = "module"
projects[rules][subdir] = "contrib/"

;; current release always good ;;
projects[entity][type] = "module"
projects[entity][subdir] = "contrib/"

;; current release always good ;;
projects[entityreference][type] = "module"
projects[entityreference][subdir] = "contrib/"

;; current release always good ;;
projects[entityreference_prepopulate][type] = "module"
projects[entityreference_prepopulate][subdir] = "contrib/"

projects[services][version] = 3.7
projects[services][subdir] = "contrib/"
projects[services][type] = "module"

projects[jquery_update][version] = 2.4
projects[jquery_update][subdir] = "contrib/"
projects[jquery_update][type] = "module"

projects[hybridauth][subdir] = "contrib/"
projects[hybridauth][type] = "module"

projects[r4032login][subdir] = "contrib/"
projects[r4032login][type] = "module"

projects[content_access][subdir] = "contrib/"
projects[content_access][type] = "module"

;;
;; UI
;;

projects[superfish][type] = "module"
projects[superfish][subdir] = "contrib/"

projects[menu_block][type] = "module"
projects[menu_block][subdir] = "contrib/"

;;
;; Themes
;;

projects[zen][type] = "theme"
projects[zen][version] = 5.4

;;
;; Admin tools
;;

projects[module_filter][version] = 1.7
projects[module_filter][type] = "module"
projects[module_filter][subdir] = "contrib/"

projects[coffee][version] = 2.0
projects[coffee][type] = "module"
projects[coffee][subdir] = "contrib/"

projects[libraries][version] = 2.1
projects[libraries][type] = "module"
projects[libraries][subdir] = "contrib/"

projects[backup_migrate][version] = 3.0
projects[backup_migrate][type] = "module"
projects[backup_migrate][subdir] = "contrib/"

projects[features][version] = 2.0-beta1
projects[features][type] = "module"
projects[features][subdir] = "contrib/"

projects[strongarm][version] = 2.0
projects[strongarm][type] = "module"
projects[strongarm][subdir] = "contrib/"

projects[features_extra][download][revision] = "4c73d638cd0a7f507b20b8b99a42207085ab9380"
projects[features_extra][type] = "module"
projects[features_extra][subdir] = "contrib/"

projects[diff][version] = 3.2
projects[diff][type] = "module"
projects[diff][subdir] = "contrib/"

projects[workbench][version] = 1.2
projects[workbench][type] = "module"
projects[workbench][subdir] = "contrib/"

;;
;; Modules for online training system
;;

projects[pdf_forms][version] = 1.x-dev
projects[pdf_forms][type] = "module"
projects[pdf_forms][subdir] = "contrib/"

projects[fillpdf][version] = 2.x-dev
projects[fillpdf][type] = "module"
projects[fillpdf][subdir] = "contrib/"

projects[token][type] = "module"
projects[token][subdir] = "contrib/"

projects[quiz][version] = 4.0-beta2
projects[quiz][type] = "module"
projects[quiz][subdir] = "contrib/"

projects[uuid][version] = 1.0-alpha5
projects[uuid][download][type] = "git"
projects[uuid][type] = "module"
projects[uuid][subdir] = "contrib/"

projects[outline_designer][version] = 2.0-beta2
projects[outline_designer][type] = "module"
projects[outline_designer][subdir] = "contrib/"

projects[custom_pagers][download][revision] = "dcc774741f5152a8c6a10d21a70399d77794cfa2"
projects[custom_pagers][download][type] = "git"
projects[custom_pagers][type] = "module"
projects[custom_pagers][subdir] = "contrib/"

projects[book_helper][version] = 1.x-dev
projects[book_helper][type] = "module"
projects[book_helper][subdir] = "contrib/"

projects[training_site_module][download][type] = "git"
projects[training_site_module][download][url] = "myrepo:eclerico/training_site_module.git"
projects[training_site_module][type] = "module"
projects[training_site_module][subdir] = "custom/"

;; currently the rules_for_quiz modules is a sandbox project
projects[rules_for_quiz][download][type] = "git"
projects[rules_for_quiz][download][branch] = master 
projects[rules_for_quiz][download][url] = sandbox:sandbox/ppc.coder/1303232.git 
projects[rules_for_quiz][type] = "module"
projects[rules_for_quiz][subdir] = "custom/"

;;
;; Libraries
;;

libraries[ckeditor][download][type] = get
libraries[ckeditor][download][url] = http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.3.1/ckeditor_4.3.1_full.zip
libraries[ckeditor][directory_name] = ckeditor

libraries[plupload][download][type] = get
libraries[plupload][download][url] = https://github.com/moxiecode/plupload/archive/v1.5.8.zip
libraries[plupload][directory_name] = plupload

libraries[htmlpurifier][download][type] = get
libraries[htmlpurifier][download][url] = http://htmlpurifier.org/releases/htmlpurifier-4.6.0.zip
libraries[htmlpurifier][directory_name] = htmlpurifier

libraries[hybridauth][download][type] = get
libraries[hybridauth][download][url] = http://sourceforge.net/projects/hybridauth/files/latest/download
libraries[hybridauth][directory_name] = hybridauth

